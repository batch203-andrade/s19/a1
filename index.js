let username;
let password;
let role;

function login() {
    username = prompt("Enter your username: ");
    password = prompt("Enter your password: ");
    role = prompt("Enter your role: ").toLowerCase();

    if (
        username === '' || username === null ||
        password === '' || password === null ||
        role === '' || role === null
    ) {
        alert('Input must not be empty');
        login();
    } else {
        switch (role) {
            case 'admin':
                alert('Welcome back to the class portal, ' + role + '!');
                break;
            case 'teacher':
                alert('Thank you for logging in,' + role + '!');
                break;
            case 'student':
                alert('Welcome to the class portal,' + role + '!');
                break;
            default:
                alert('Role out of range');
                login();
        }
    }
}

login();

//GET AVERAGE OF GRADES AND PASS IT TO ITS GRADE EQUIVALENT
const checkAverage = (...grades) => getLetterGradeEquivalent(Math.round((grades.reduce((acc, sum) => acc + sum, 0)) / grades.length));

const getLetterGradeEquivalent = function (grade) {
    if (grade <= 74) {
        return console.log('Hello, student, your average is ' + grade + '. The letter equivalent is F');
    }
    else if (grade >= 75 && grade <= 79) {
        return console.log('Hello, student, your average is ' + grade + '. The letter equivalent is D');
    }
    else if (grade >= 80 && grade <= 84) {
        return console.log('Hello, student, your average is ' + grade + '. The letter equivalent is C');
    }
    else if (grade >= 85 && grade <= 89) {
        return console.log('Hello, student, your average is ' + grade + '. The letter equivalent is B');
    }
    else if (grade >= 90 && grade <= 95) {
        return console.log('Hello, student, your average is ' + grade + '. The letter equivalent is A');
    }
    else if (grade >= 96 && grade <= 100) {
        return console.log('Hello, student, your average is ' + grade + '. The letter equivalent is A+');
    } else {
        return console.log('One of the grades that you inputted is not valid, hence getting an average greater than 100');
    }
}

console.log('checkAverage(71, 70, 73, 74)');
checkAverage(71, 70, 73, 74);
console.log('checkAverage(75, 75, 76, 78)');
checkAverage(75, 75, 76, 78);
console.log('checkAverage(80, 81, 82, 78)');
checkAverage(80, 81, 82, 78);
console.log('checkAverage(84, 85, 87, 88)');
checkAverage(84, 85, 87, 88);
console.log('checkAverage(89, 90, 91, 90)');
checkAverage(89, 90, 91, 90);
console.log('checkAverage(91, 96, 97, 95)');
checkAverage(91, 96, 97, 95);